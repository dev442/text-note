import React, { Fragment, useEffect } from 'react';
import config from 'visual-config-exposer';

import './Note.css';

let alignment = '';
if (config.settings.alignment === 'center') {
  alignment = 'center';
} else if (config.settings.alignment === 'left') {
  alignment = 'flex-start';
} else if (config.settings.alignment === 'right') {
  alignment = 'flex-end';
}

console.log(config);

const Note = () => {
  useEffect(function () {
    animateSequence();
  });

  function animateSequence() {
    let i, j;
    let a = document.getElementsByClassName('sequence');
    let stringElement = a[0];
    let letter = stringElement.innerHTML;
    letter = letter.trim();
    var str = '';
    let delay = 100;
    let stringArr = letter.split('');
    str +=
      '<span style="animation-delay:' +
      delay +
      'ms; -moz-animation-delay:' +
      delay +
      'ms; -webkit-animation-delay:' +
      delay +
      'ms; ">'; // +
    // '</span>';
    for (j = 0; j < letter.length; j++) {
      if (letter[j] != ' ') {
        str +=
          '<span style="animation-delay:' +
          delay +
          'ms; -moz-animation-delay:' +
          delay +
          'ms; -webkit-animation-delay:' +
          delay +
          'ms; ">' +
          letter[j] +
          '</span>';
        delay += 70;
      } else if (letter[j] === ' ') {
        str +=
          '</span>' +
          ' ' +
          '<span style="animation-delay:' +
          delay +
          'ms; -moz-animation-delay:' +
          delay +
          'ms; -webkit-animation-delay:' +
          delay +
          'ms; ">';
        delay += 40;
      }
    }
    str += '</span>';
    stringElement.innerHTML = str;
  }

  const skipAnimations = () => {
    let element = document.getElementsByClassName('sequence');
    let stringElement = element[0];
    stringElement.innerHTML = config.settings.content;
  };

  return (
    <Fragment>
      <main className="name__container">
        <button className="btn__skip" onClick={skipAnimations}>
          <img className="btn__img" src={config.settings.eyeImg} alt="eye" />
        </button>
        <article>
          <h1 className="cssanimation sequence leFlyInBottom">
            {config.settings.content}
          </h1>
        </article>
      </main>
      <style>
        {`

      .name__container {
        background : url(${config.settings.bgImg});
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        align-items: ${alignment};
      }

      h1 {
        font-size: ${config.settings.fontSize};
        color: ${config.settings.fontColor};
      }
  `}
      </style>
    </Fragment>
  );
};

export default Note;
